run:
	ansible-playbook emba.yml -i inventory

server:
	ansible-playbook emba.yml -i inventory --limit server

runners:
	ansible-playbook emba.yml -i inventory --limit runners
